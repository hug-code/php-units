<?php

if (!function_exists('_array_field')) {
    /**
     * @Desc 获取数组字段值
     * @param array $array
     * @param string $field
     * @param bool|string|integer $default 默认值
     * @param bool|string|integer|array $exception 例外值
     * @return bool|mixed
     * @author yashuai<1762910894@qq.com>
     */
    function _array_field(array $array = [], string $field = '', $default = false, $exception = true)
    {
        return \HugCode\PhpUnits\Utils\UtilArray::arrayField($array, $field, $default, $exception);
    }
}

if (!function_exists('_params_create_where')) {
    /**
     * @Desc 参数生成where
     * @param array $params
     * @param array $fieldList
     * @param array $where
     * @return array
     * @author yashuai<1762910894@qq.com>
     */
    function _params_create_where(array $params, array $fieldList, array $where = [])
    {
        return \HugCode\PhpUnits\Frame\DB\Where::paramsCreateWhere($params, $fieldList, $where);
    }
}

if (!function_exists('_params_where_between_day')) {
    /**
     * @Desc between 时间条件（时间戳转时间格式 Ymd
     * @param array $params
     * @param string $field
     * @param int $default
     * @param array $where
     * @return array
     * @author yashuai<1762910894@qq.com>
     */
    function _params_where_between_day(array $params, string $field, int $default = 0, array $where = [])
    {
        return \HugCode\PhpUnits\Frame\DB\Where::paramsWhereBetweenDay($params, $field, $default, $where);
    }
}


if (!function_exists('_params_isset_table_data')) {
    /**
     * @Desc   判断参数是否存在并赋值
     * @param array $data
     * @param array $params
     * @param array $field
     * @return array
     * @author yashuai<1762910894@qq.com>
     */
    function _params_isset_table_data(array $field = [], array $params = [], array $data = [])
    {
        foreach ($field as $key) {
            if (isset($params[$key])) {
                $data[$key] = $params[$key];
            }
        }
        return $data;
    }
}

if (!function_exists('_to_under_score')) {
    /**
     * @Desc   驼峰命名转下划线命名
     * @param string $name
     * @param string $separator
     * @return string
     * @author yashuai<1762910894@qq.com>
     */
    function _to_under_score(string $name = '', string $separator = '-')
    {
        return strtolower(preg_replace('/([a-z])([A-Z])/', "$1" . $separator . "$2", $name));
    }
}

if (!function_exists('_words_capitalized')) {
    /**
     * @Desc 单词首字母大写
     * @param string $str
     * @param string $search
     * @param string $replace
     * @return string|string[]
     * @author yashuai<1762910894@qq.com>
     */
    function _words_capitalized(string $str, string $search = '_', string $replace = '')
    {
        return \HugCode\PhpUnits\Utils\UtilLetter::capitalizeFirstLetter($str, $search, $replace);
    }
}
