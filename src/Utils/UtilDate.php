<?php
/**
 * @Name: UtilDate.php
 * @Author: yashuai<1762910894@qq.com>
 */

namespace HugCode\PhpUnits\Utils;

class UtilDate
{

    /**
     * @Desc 将秒数转为 H:i:s
     * @param int $seconds
     * @return string
     * @author yashuai<1762910894@qq.com>
     */
    public static function formatSecond(int $seconds)
    {
        $format = $seconds < 3600 ? '%M:%S' : '%H:%M:%S';
        return gmstrftime($format, $seconds);
    }

    /**
     * @Desc 计算两个日期相差时间
     * @param int|string $start
     * @param int|string $end
     * @param int $type 0:天数  1:小时数  2:分钟数 3:秒数
     * @return float|int
     * @author yashuai<1762910894@qq.com>
     */
    public static function diffBetweenDays($start, $end, int $type = 0)
    {
        $interval = [86400, 3600, 60, 0];
        $type     = $interval[$type] ?? $interval[0];
        $res      = strtotime($end) - strtotime($start);
        return $type ? (int)$res / $type : $res;
    }

    /**
     * @Desc 当前微秒值
     * @return float
     * @author yashuai<1762910894@qq.com>
     */
    public static function microsecond()
    {
        return (float)sprintf('%.0f', (microtime(TRUE)) * 1000);
    }

    /**
     * @Desc 日期语义化
     * @param int $time
     * @return string
     * @author yashuai<1762910894@qq.com>
     */
    public static function format(int $time)
    {
        $interval = null;
        $second   = time() - $time;
        if ($second <= 0) {
            $second = 0;
        }
        if ($second < 60) {
            $interval = "刚刚";
        } else if ($second < 60 * 60) { // 大于1分钟 小于1小时
            $minute   = $second / 60;
            $interval = $minute . "分钟前";
        } else if ($second >= 60 * 60 && $second < 60 * 60 * 24) { // 大于1小时 小于24小时
            $hour = ($second / 60) / 60;
            if ($hour <= 3) {
                $interval = $hour . "小时前";
            } else {
                $interval = "今天" . date('H:i', $time);
            }
        } else if ($second >= 60 * 60 * 24 && $second <= 60 * 60 * 24 * 2) { // 大于1D 小于2D
            $interval = "昨天" . date('H:i', $time);
        } else if ($second >= 60 * 60 * 24 * 2 && $second <= 60 * 60 * 24 * 7) { // 大于2D小时 小于 7天
            $day      = (($second / 60) / 60) / 24;
            $interval = $day . "天前";
        } else if ($second <= 60 * 60 * 24 * 365 && $second >= 60 * 60 * 24 * 7) { // 大于7天小于365天
            $interval = date('m-d H:i', $time);
        } else if ($second >= 60 * 60 * 24 * 365) { // 大于365天
            $interval = date('Y-m-d H:i:s', $time);
        }
        return $interval;
    }

    /**
     * @Desc 获取当月起止时间
     * @param string $format
     * @return array
     * @author yashuai<1762910894@qq.com>
     */
    public static function getMonthStartOrEnd(string $format = 'Ymd')
    {
        $start = mktime(0, 0, 0, date('m'), 1, date('Y'));
        $end   = mktime(23, 59, 59, date('m'), date('t'), date('Y'));
        return [
            'Start' => empty($format) ? $start : date($format, $start),
            'End'   => empty($format) ? $end : date($format, $end),
        ];
    }

    /**
     * @Desc 获取两个时间范围内所有的时间
     * @param $start
     * @param $end
     * @param string $format
     * @param bool $complete 是否包含起止时间
     * @return array
     * @author yashuai<1762910894@qq.com>
     */
    public static function getDayRangeFrom($start, $end, string $format = 'Ymd', bool $complete = false)
    {
        if (empty($start) || empty($end)) {
            return [];
        }
        $dt_start = strtotime($start);
        $dt_end   = strtotime($end);
        if ($dt_start > $dt_end) {
            return [];
        }

        $response = [];
        while ($dt_start < $dt_end) {
            $dt_start = strtotime('+1 day', $dt_start);
            if ($dt_start != $dt_end) {
                array_push($response, self::dateFormat($dt_start, $format));
            }
        }
        if ($complete) {
            array_push($response, self::dateFormat($dt_end, $format));
            array_unshift($response, self::dateFormat(strtotime($start), $format));
        }
        return $response;
    }

    /**
     * @Desc 时间格式化
     * @param string|int $data
     * @param string|false $format
     * @return false|int|string
     * @author yashuai<1762910894@qq.com>
     */
    public static function dateFormat($data = 0, $format = '')
    {
        return empty($format) || empty($data) ? $data : date($format, $data);
    }

    /**
     * @Desc 获取问候语
     * @return string
     * @author yashuai<1762910894@qq.com>
     */
    public static function getHello()
    {
        $h = date('H');
        if ($h < 9 && $h > 5) {
            return "早上好";
        } elseif ($h < 12) {
            return '上午好';
        } elseif ($h < 14) {
            return '中午好';
        } elseif ($h < 19) {
            return '下午好';
        } elseif ($h < 22) {
            return '晚上好';
        } else {
            return '夜深了，要注意休息哦';
        }
    }

}
