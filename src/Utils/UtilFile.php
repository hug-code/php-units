<?php
/**
 * @Name: UtilFile.php
 * @Author: yashuai<1762910894@qq.com>
 */

namespace HugCode\PhpUnits\Utils;

class UtilFile
{

    /**
     * @Desc 生成目录
     * @param string $dir
     * @return bool
     * @author yashuai<1762910894@qq.com>
     */
    public static function mkdir(string $dir)
    {
        if (is_dir($dir)) {
            return true;
        }
        if (!mkdir($dir, 0755, true)) {
            return false;
        }
        @chmod($dir, 0755);
        return true;
    }

    /**
     * @Desc 删除文件
     * @param string $filename
     * @author yashuai<1762910894@qq.com>
     */
    public static function deleteFile(string $filename)
    {
        if (file_exists($filename)) {
            unlink($filename);
        }
    }

    /**
     * @Desc Desc 格式化字节大小
     * @param int $size 字节数
     * @param string $delimiter 数字和单位分隔符
     * @return string            格式化后的带单位的大小
     * @author yashuai<1762910894@qq.com>
     */
    public static function formatBytes(int $size, string $delimiter = '')
    {
        $units = array('B', 'KB', 'MB', 'GB', 'TB', 'PB');
        for ($i = 0; $size >= 1024 && $i < 5; $i++) $size /= 1024;
        return round($size, 2) . $delimiter . $units[$i];
    }

    /**
     * @Desc 获取包含子目录下的所有文件列表
     * @param $dir
     * @return array
     * @author yashuai<1762910894@qq.com>
     */
    public static function includeSubFileList($dir)
    {
        $files = [];
        if (@$handle = opendir($dir)) {
            while (($file = readdir($handle)) !== false) {
                if ($file != ".." && $file != ".") {
                    if (is_dir($dir . "/" . $file)) {
                        $files = array_merge($files, self::includeSubFileList($dir . "/" . $file));
                    } else {
                        $files[] = $dir . "/" . $file;
                    }
                }
            }
            closedir($handle);
        }
        return $files;
    }

}
