<?php
/**
 * @Name: UtilLetter.php
 * @Author: yashuai<1762910894@qq.com>
 */

namespace HugCode\PhpUnits\Utils;

class UtilLetter
{

    /**
     * @Desc 单词首字母大写
     * @param string $str
     * @param string $search 单词分割符
     * @param string $replace 返回的单词分隔符
     * @return string|string[]
     * @author yashuai<1762910894@qq.com>
     */
    public static function capitalizeFirstLetter(string $str, string $search = '_', string $replace = '')
    {
        $str = str_replace($search, ' ', $str);
        $str = ucwords($str);
        return str_replace(' ', $replace, $str);
    }

}
