<?php
/**
 * @Name: UtilFormat.php
 * @Author: yashuai<1762910894@qq.com>
 */

namespace HugCode\PhpUnits\Utils;

class UtilFormat
{

    /**
     * @Desc 是否为json格式的string
     * @param string $str
     * @return bool
     * @author yashuai<1762910894@qq.com>
     */
    public static function isJsonString(string $str)
    {
        $str = trim($str);
        if (empty($str)) return false;
        try {
            json_decode($str, true);
            return JSON_ERROR_NONE === json_last_error();
        } catch (\Exception $e) {
            return false;
        }
    }

}
