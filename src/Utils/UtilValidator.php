<?php
/**
 * @Name: UtilValidator.php
 * @Author: yashuai<1762910894@qq.com>
 */

namespace HugCode\PhpUnits\Utils;

class UtilValidator
{

    /**
     * @Desc 是否为url
     * @param mixed $value
     * @return bool
     * @author yashuai<1762910894@qq.com>
     */
    public static function isUrl($value)
    {
        if (empty($value)) {
            return false;
        }
        return false !== filter_var($value, FILTER_VALIDATE_URL);
    }

    /**
     * @Desc 是否为邮箱
     * @param mixed $value
     * @return bool
     * @author yashuai<1762910894@qq.com>
     */
    public static function isEmail($value)
    {
        if (empty($value)) {
            return false;
        }
        return false !== filter_var($value, FILTER_VALIDATE_EMAIL);
    }


    /**
     * @Desc 判断是否为身份证格式
     * @param mixed $value
     * @return bool
     * @author yashuai<1762910894@qq.com>
     */
    public static function isCardCode($value)
    {
        return self::filterRegexp($value, '/^[0-9a-zA-Z]{15}$|^[0-9a-zA-Z]{18}$/iu');
    }

    /**
     * @Desc 判断是否为手机号码
     * @param mixed $value
     * @return bool
     * @author yashuai<1762910894@qq.com>
     */
    public static function isMobile($value)
    {
        return self::filterRegexp($value, '/^1\d{10}$/');
    }

    /**
     * @Desc 判断是否为纯数字
     * @param mixed $value
     * @param int $min
     * @param int $max
     * @return bool
     * @author yashuai<1762910894@qq.com>
     */
    public static function isInt($value, int $min = 0, int $max = 0)
    {
        if (empty($value)) {
            return false;
        }
        $option = [];
        if (!empty($min) && !empty($max)) {
            $option = [
                'options' => [
                    'min_range' => $min,
                    'max_range' => $max,
                ]
            ];
        }
        return false !== filter_var($value, FILTER_VALIDATE_INT, $option);
    }

    /**
     * @Desc 是否为浮点数
     * @param mixed $value
     * @return bool
     * @author yashuai<1762910894@qq.com>
     */
    public static function isFloat($value)
    {
        if (empty($value)) {
            return false;
        }
        return false !== filter_var($value, FILTER_VALIDATE_FLOAT);
    }

    /**
     * @Desc 判断是否为纯英文
     * @param mixed $value
     * @return bool
     * @author yashuai<1762910894@qq.com>
     */
    public static function isEng($value)
    {
        return self::filterRegexp($value, '/^[a-z]*$/i');
    }

    /**
     * @Desc 是否为邮编
     * @param $value
     * @return bool
     * @author yashuai<1762910894@qq.com>
     */
    public static function isZipCode($value)
    {
        return self::filterRegexp($value, '/^[1-9][0-9]{5}$/');
    }

    /**
     * @Desc 匹配中文
     * @param $value
     * @return bool
     * @author yashuai<1762910894@qq.com>
     */
    public static function chinese($value)
    {
        return self::filterRegexp($value, '/^[\x{4e00}-\x{9fa5}]+$/u');
    }

    /**
     * @Desc 长度是否在范围   中文UTF8占3个字符
     * @param string $value
     * @param int $min
     * @param int $max
     * @return bool
     * @author yashuai<1762910894@qq.com>
     */
    public static function strLengthRange(string $value, int $min, int $max)
    {
        if (empty($value)) {
            return false;
        }
        $len = strlen($value);
        if ($len < $min || $len > $max) {
            return false;
        }
        return true;
    }

    /**
     * @Desc 正则验证
     * @param $value
     * @param $regexp
     * @return bool
     * @author yashuai<1762910894@qq.com>
     */
    public static function filterRegexp($value, $regexp)
    {
        if (empty($value)) {
            return false;
        }
        $option = [
            'options' => ['regexp' => $regexp]
        ];
        return false !== filter_var($value, FILTER_VALIDATE_REGEXP, $option);
    }

}
