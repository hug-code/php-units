<?php
/**
 * @Name: UtilRandom.php
 * @Author: yashuai<1762910894@qq.com>
 */

namespace HugCode\PhpUnits\Utils;

class UtilRandom
{

    /**
     * @Desc 生成随机手机号
     * @param int $len
     * @return array|string
     * @author yashuai<1762910894@qq.com>
     */
    public static function phoneNumber(int $len = 1)
    {
        $segment = [
            '130', '131', '132', '133', '134', '135', '136', '137', '138', '139',
            '144', '147',
            '150', '151', '152', '153', '155', '156', '157', '158', '159',
            '176', '177', '178',
            '180', '181', '182', '183', '184', '185', '186', '187', '188', '189',
        ];
        if ($len === 1) {
            return $segment[array_rand($segment)] . mt_rand(1000, 9999) . mt_rand(1000, 9999);
        } else {
            $temp = [];
            for ($i = 0; $i < $len; $i++) {
                $temp[] = $segment[array_rand($segment)] . mt_rand(1000, 9999) . mt_rand(1000, 9999);
            }
            return array_unique($temp);
        }
    }

}
