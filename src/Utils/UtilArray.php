<?php
/**
 * @Name: UtilArray.php
 * @Author: yashuai<1762910894@qq.com>
 */

namespace HugCode\PhpUnits\Utils;

class UtilArray
{

    /**
     * @Desc  把返回的数据集转换成Tree
     * @param array $list 原始数据
     * @param string $pk 主键
     * @param string $pid 父级ID
     * @param string $child 子级索引
     * @return array
     * @author yashuai<1762910894@qq.com>
     */
    public static function listToTree(array $list = [], string $pk = 'id', string $pid = 'pid', string $child = '_child')
    {
        $tree = array();  //格式化好的树
        foreach ($list as $item) {
            if (isset($list[$item[$pid]])) {
                $list[$item[$pid]][$child][] = &$list[$item[$pk]];
            } else {
                $tree[] = &$list[$item[$pk]];
            }
        }
        return $tree;
    }

    /**
     * @Desc  获取数组字段值
     * @param array $array 数组
     * @param string $field 字段名称
     * @param bool|string|integer $default 默认值
     * @param bool|string|integer|array $exception 例外值
     * @return bool|mixed
     * @author yashuai<1762910894@qq.com>
     */
    public static function arrayField(array $array = [], string $field = '', $default = false, $exception = true)
    {
        if (!isset($array[$field])) {
            return $default;
        }
        $value = $array[$field];
        return empty($value) ? ($value === $exception ? $value : $default) : $value;
    }

    /**
     * @Desc 二维数组转key=>val
     * @param array $arr
     * @param string $key
     * @param string $val
     * @return array|false
     * @author yashuai<1762910894@qq.com>
     */
    public static function arrayToKeyVal(array $arr, string $key, string $val)
    {
        return array_combine(array_column($arr, $key), array_column($arr, $val));
    }

    /**
     * @Desc 通过值删除
     * @param array $data
     * @param string $value
     * @return array
     * @author yashuai<1762910894@qq.com>
     */
    public static function deleteByValue(array $data, string $value = '')
    {
        $idx = array_search($value, $data);
        if ($idx === false) return $data;
        unset($data[$idx]);
        return $data;
    }

}
