<?php
/**
 * @Name: Password.php
 * @Author: yashuai<1762910894@qq.com>
 */

namespace HugCode\PhpUnits\CipherText;

class Password
{

    /**
     * CMF密码加密方法
     * @param string $password 要加密的字符串
     * @param string $key 加密秘钥
     * @return string
     */
    public static function generatePassword(string $password = '', string $key = '')
    {
        return "###" . md5(md5($password . $key));
    }

    /**
     * 密码比较方法,所有涉及密码比较的地方都用这个方法
     * @param string $password 要比较的密码
     * @param string $passwordInDb 已经加密过的密码
     * @param string $key 加密秘钥
     * @return boolean 密码相同，返回true
     */
    public static function validatePassword(string $password = '', string $passwordInDb = '', string $key = '')
    {
        return self::generatePassword($password, $key) == $passwordInDb;
    }

}
