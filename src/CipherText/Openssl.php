<?php
/**
 * @Name: Openssl.php
 * @Author: yashuai<1762910894@qq.com>
 */

namespace HugCode\PhpUnits\CipherText;

class Openssl
{

    /**
     * @var
     */
    public $private_key = null;

    /**
     * @var
     */
    public $public_key = null;

    /**
     * @Desc 获取私钥
     * @param string $keyPath
     * @author yashuai<1762910894@qq.com>
     */
    public function setPrivateKey(string $keyPath = '')
    {
        if (file_exists($keyPath)) {
            $this->private_key = openssl_pkey_get_private(file_get_contents($keyPath));
        }
    }

    /**
     * @Desc 获取公钥
     * @param string $keyPath
     * @author yashuai<1762910894@qq.com>
     */
    public function setPublicKey(string $keyPath = '')
    {
        if (file_exists($keyPath)) {
            $this->public_key = openssl_pkey_get_public(file_get_contents($keyPath));
        }
    }

    /**
     * @Desc 私钥解密
     * @param string $encrypted
     * @return string
     * @author yashuai<1762910894@qq.com>
     */
    public function privateDecrypt(string $encrypted)
    {
        if(empty($this->private_key)){
            return '';
        }
        $crypto = '';
        foreach (str_split(Utils::base64UrlDecode($encrypted), 128) as $chunk) {
            openssl_private_decrypt($chunk, $decryptData, $this->private_key, OPENSSL_PKCS1_PADDING);
            $crypto .= $decryptData;
        }
        return $crypto;
    }

    /**
     * @Desc 私钥加密
     * @param string $data
     * @return string|string[]
     * @author yashuai<1762910894@qq.com>
     */
    public function privateEncrypt(string $data)
    {
        if(empty($this->private_key)){
            return '';
        }
        $crypto = '';
        foreach (str_split($data, 117) as $chunk) {
            openssl_private_encrypt($chunk, $decryptData, $this->private_key, OPENSSL_PKCS1_PADDING);
            $crypto .= $decryptData;
        }
        return Utils::base64UrlEncode($crypto);
    }

    /**
     * @Desc 公钥解密
     * @param string $encrypted
     * @return string
     * @author yashuai<1762910894@qq.com>
     */
    public function publicDecrypt(string $encrypted)
    {
        if(empty($this->public_key)){
            return '';
        }
        $crypto = '';
        foreach (str_split(Utils::base64UrlDecode($encrypted), 128) as $chunk) {
            openssl_public_decrypt($chunk, $decryptData, $this->public_key, OPENSSL_PKCS1_PADDING);
            $crypto .= $decryptData;
        }
        return $crypto;
    }

    /**
     * @Desc 公钥加密
     * @param string $data
     * @return string|string[]
     * @author yashuai<1762910894@qq.com>
     */
    public function publicEncrypt(string $data)
    {
        if(empty($this->public_key)){
            return '';
        }
        $crypto = '';
        foreach (str_split($data, 117) as $chunk) {
            openssl_public_encrypt($chunk, $decryptData, $this->public_key, OPENSSL_PKCS1_PADDING);
            $crypto .= $decryptData;
        }
        return Utils::base64UrlEncode($crypto);
    }

}



