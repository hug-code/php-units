<?php
/**
 * @name: 单例
 * @Created by PhpStorm
 * @file: InstanceTool.php
 */

namespace HugCode\PhpUnits;

trait InstanceTool
{

    public $params;

    /**
     * Instances of the derived classes.
     * @var array
     */
    protected static $instances = array();

    /**
     * Get instance of the derived class.
     * @param array $params
     * @return static
     */
    public static function instance(array $params = [])
    {
        $className = get_called_class();
        if (!isset(self::$instances[$className])) {
            self::$instances[$className] = new $className;
        }
        self::$instances[$className]->params = $params;
        return self::$instances[$className];
    }

    /**
     * 该私有对象阻止实例被克隆
     */
    public function __clone()
    {
    }

    /**
     * 该私有方法阻止实例被序列化
     */
    public function __wakeup()
    {
    }

}
