<?php
/**
 * @Name: ConstCode.php
 * @Author: yashuai<1762910894@qq.com>
 * @Desc    常量定义
 */

namespace HugCode\PhpUnits\Frame\Code;

class ConstCode
{

    // 基本状态  特殊情况另外定义
    const BASIC_STATUS_NORMAL = 1; //正常状态
    const BASIC_STATUS_DELETE = 2; //删除(作废)状态

    // 基本判断类型 特殊情况另外定义
    const IS_NO = 0;   // 否
    const IS_YES = 1;  // 是
    const BOOL = [
        self::IS_NO  => '否',
        self::IS_YES => '是'
    ];

    // 菜单类型
    const NODE_TYPE_MENU = 1;
    const NODE_TYPE_BUTTON = 2;
    const NODE_TYPE = [
        self::NODE_TYPE_MENU   => '菜单',
        self::NODE_TYPE_BUTTON => '按钮'
    ];

}
