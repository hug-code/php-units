<?php
/**
 * @Name: ResponseCode.php
 * @Author: yashuai<1762910894@qq.com>
 * @Desc    公共状态码管理
 */

namespace HugCode\PhpUnits\Frame\Code;

class ResponseCode
{

    /**
     * @param int $code
     * @return string
     * @author yashuai<1762910894@qq.com>
     * @desc code码获取中文注释
     */
    public static function CodeGetMsg(int $code = 0) :string
    {
        $codes = [
            // 基础
            0      => 'success',
            105000 => '请求方式不合法',
            105001 => '接口正在维护',
            105002 => '程序执行异常',      // 用于 try catch 捕获到异常时抛出

            // 数据类
            106000 => '暂无数据',
            106001 => '当前记录不存在',
            106002 => '参数验证异常',

            // 用户类
            107000 => '登录状态异常',
            107001 => '当前账号不存在',
            107002 => '账号或密码错误',
            107003 => '当前账户状态异常，无法登录',
            107004 => '您无权限访问',
            107005 => '登录已过期',
            107006 => '您的账号已在其他设备登录',
            107007 => '您的账号已被禁用，请联系管理员',
            107008 => '请完善资料',
        ];
        return $codes[$code] ?? '';
    }


}
